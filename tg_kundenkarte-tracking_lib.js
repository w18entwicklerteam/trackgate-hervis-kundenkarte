
tgNamespace.tgContainer.registerInnerGate("kundenkarte-tracking", function(oWrapper, oLocalConfData)
{

    this.oWrapper = oWrapper;
    this.oLocalConfData = oLocalConfData;
    this.redirectAction;



    this._construct = function()
    {
        if(this.oWrapper.debug("kundenkarte-tracking","_construct"))debugger;

        this.oWrapper.registerInnerGateFunctions("kundenkarte-tracking","form_submit",this.trackFormSubmit);

    }


    //Hängt sich an den Form-Submit Funktion des Formular-Tracking und sendet einen Event-Befehl an UA-Gate

    this.trackFormSubmit = function (oArgs)
{
    if(this.oWrapper.debug("kundenkarte-tracking","trackFormSubmit"))debugger;

    this.redirectAction = location.href;

    if(this.redirectAction !== "undefined" && this.oLocalConfData.link.test(this.redirectAction) === true){

        var plz = document.getElementById("postcode").value;



        var regexRedirect;

        if(oLocalConfData.regex.test(location.href)===true){
            var currentUrl = new URL(location.href);
            regexRedirect= currentUrl.searchParams.get("redirectsource");
            regexRedirect = regexRedirect.substring(0, regexRedirect.indexOf('.'));
        }

        switch(regexRedirect){
            case("club"):
                this.oWrapper.exeFunction({
                    track: "event",
                    group: "Kundenkarte",
                    name: "club",
                    value: plz
                });
                break;

            case("card"):
                this.oWrapper.exeFunction({
                    track: "event",
                    group: "Kundenkarte",
                    name: "card",
                    value: plz
                });
                break;

            case("join"):
                this.oWrapper.exeFunction({
                    track: "event",
                    group: "Kundenkarte",
                    name: "join",
                    value: plz
                });
                break;

            case(null):
            case("undefined"):
            case(""):
            case(undefined):
                this.oWrapper.exeFunction({
                    track: "event",
                    group: "Kundenkarte",
                    name: "hervis",
                    value: plz
                });
                break;


        }

    }


}


});

